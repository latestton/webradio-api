package com.gigabyte.api.webradio.service

import com.gigabyte.api.webradio.model.Country
import com.gigabyte.api.webradio.repository.CountryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class CountryService {
    @Autowired lateinit var repository: CountryRepository
    fun getAllCountries(): Iterable<Country> = repository.findAll()
    fun getCountryById(id : Int): Optional<Country> = repository.findById(id)
    fun  saveCountry(country : Country):Country = repository.save(country)
    fun  delete(country : Country) = repository.delete(country)
    fun  deleteCountryById(id : Int): Unit = repository.deleteById(id)
}