package com.gigabyte.api.webradio.service

import com.gigabyte.api.webradio.model.Category
import com.gigabyte.api.webradio.model.Channel
import com.gigabyte.api.webradio.repository.ChannelRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class ChannelService {
    @Autowired lateinit var repository: ChannelRepository
    fun getAllChannels(): Iterable<Channel> = repository.findAll()
    fun getChannelById(id: Int): Optional<Channel> = repository.findById(id)
    fun saveChannel(channel: Channel): Channel = repository.save(channel)
    fun deleteChannel(channel: Channel) = repository.delete(channel)
    fun deleteChannelById(id: Int) = repository.deleteById(id)
    fun updateChannel(channel: Channel) = repository.save(channel)
    fun getChannelByName(name: String): Optional<Channel> = repository.findByName(name)
    fun getChannelsByCategory(category: Category): Iterable<Channel> = repository.findByCategory(category)

}