package com.gigabyte.api.webradio.service

import com.gigabyte.api.webradio.model.Promoteur
import com.gigabyte.api.webradio.repository.PromoteurRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class PromoteurService {
    @Autowired
    lateinit var repository : PromoteurRepository
    fun savePromoteur(promoteur : Promoteur) : Promoteur = repository.save(promoteur)
    fun getPromoteurById(id : Int) : Optional<Promoteur> = repository.findById(id)
    fun getAllPromoteurs() : Iterable<Promoteur> = repository.findAll()
    fun delete(promoteur : Promoteur) : Unit = repository.delete(promoteur)
    fun deleteById(id : Int) : Unit = repository.deleteById(id)
}