package com.gigabyte.api.webradio.service

import com.gigabyte.api.webradio.model.Owner
import com.gigabyte.api.webradio.repository.OwnerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class OwnerService {
    @Autowired lateinit var repository : OwnerRepository
    fun saveOwner(owner : Owner) : Owner = repository.save(owner)
    fun getOwnerById(id : Int) : Optional<Owner> = repository.findById(id)
    fun getAllOwners() : Iterable<Owner> = repository.findAll()
    fun delete(owner : Owner) : Unit = repository.delete(owner)
    fun deleteById(id : Int) : Unit = repository.deleteById(id)
}