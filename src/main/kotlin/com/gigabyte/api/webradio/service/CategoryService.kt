package com.gigabyte.api.webradio.service

import com.gigabyte.api.webradio.model.Category
import com.gigabyte.api.webradio.repository.CategoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.Optional

@Service
class CategoryService {
    @Autowired private lateinit var  repository : CategoryRepository
    fun getAllCategories(): Iterable<Category> =repository.findAll()
    fun getCategoryById(id: Int): Optional<Category> = repository.findById(id)
    fun saveCategory(category: Category): Category = repository.save(category)
    fun delete(category: Category) = repository.delete(category)
    fun deleteCategoryById(id: Int) = repository.deleteById(id)
    fun updateCategory(category: Category) = repository.save(category)
    fun getCategoryByName(name: String): Optional<Category> = repository.findByName(name)
}