package com.gigabyte.api.webradio.repository

import com.gigabyte.api.webradio.model.Country
import org.springframework.data.jpa.repository.JpaRepository

interface CountryRepository : JpaRepository<Country,Int > {
}