package com.gigabyte.api.webradio.repository

import com.gigabyte.api.webradio.model.Category
import com.gigabyte.api.webradio.model.Channel
import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional

interface ChannelRepository : JpaRepository<Channel,Int>{


    fun findByName(name: String): Optional<Channel>

    fun findByCategory(category: Category): Iterable<Channel>

}