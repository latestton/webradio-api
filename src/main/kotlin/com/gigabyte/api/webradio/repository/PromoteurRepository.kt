package com.gigabyte.api.webradio.repository

import com.gigabyte.api.webradio.model.Promoteur
import org.springframework.data.jpa.repository.JpaRepository

interface PromoteurRepository : JpaRepository<Promoteur,Int> {
}