package com.gigabyte.api.webradio.repository

import com.gigabyte.api.webradio.model.Category
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface CategoryRepository : JpaRepository<Category,Int> {


    fun findByName(name: String): Optional<Category>
}