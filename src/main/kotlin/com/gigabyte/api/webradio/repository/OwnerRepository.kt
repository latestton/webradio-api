package com.gigabyte.api.webradio.repository

import com.gigabyte.api.webradio.model.Owner
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface OwnerRepository : JpaRepository<Owner, Int>