package com.gigabyte.api.webradio.controller

import com.gigabyte.api.webradio.model.Country
import com.gigabyte.api.webradio.service.CountryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class CountryController {
    @Autowired
    lateinit var service : CountryService
    @PostMapping("/country") fun post(@RequestBody country: Country) = service.saveCountry(country)
    @GetMapping("/countries") fun getAllCountrys(): Iterable<Country> = service.getAllCountries()
    @GetMapping("/country/{id}") fun getCountryById(@PathVariable id: Int): Optional<Country> = service.getCountryById(id)
    @PatchMapping("/country") fun patch(@RequestBody country: Country) = service.saveCountry(country)
    @DeleteMapping("/country") fun delete(@RequestBody country: Country) = service.delete(country)
}