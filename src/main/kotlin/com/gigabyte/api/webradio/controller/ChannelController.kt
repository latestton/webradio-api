package com.gigabyte.api.webradio.controller

import com.gigabyte.api.webradio.model.Channel
import com.gigabyte.api.webradio.service.ChannelService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class ChannelController {

    @Autowired
    lateinit var service : ChannelService
    @PostMapping("/channel") fun post(@RequestBody channel: Channel) = service.saveChannel(channel)
    @GetMapping("/channels") fun getAllChannels(): Iterable<Channel> = service.getAllChannels()
    @GetMapping("/channel/{id}") fun getChannelById(@PathVariable id: Int): Optional<Channel> = service.getChannelById(id)
    @PatchMapping("/channel") fun patch(@RequestBody channel: Channel) = service.saveChannel(channel)
    @DeleteMapping("/channel") fun delete(@RequestBody channel: Channel) = service.deleteChannel(channel)
}