package com.gigabyte.api.webradio.controller

import com.gigabyte.api.webradio.model.Promoteur
import com.gigabyte.api.webradio.service.PromoteurService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class PromoteurControlleur {
    @Autowired
    lateinit var service : PromoteurService
    @PostMapping("/promoteur") fun post(@RequestBody promoteur: Promoteur) = service.savePromoteur(promoteur)
    @GetMapping("/promoteurs") fun getAllPromoteurs(): Iterable<Promoteur> = service.getAllPromoteurs()
    @GetMapping("/promoteur/{id}") fun getPromoteurById(@PathVariable id: Int): Optional<Promoteur> = service.getPromoteurById(id)
    @PatchMapping("/promoteur") fun patch(@RequestBody promoteur: Promoteur) = service.savePromoteur(promoteur)
    @DeleteMapping("/promoteur") fun delete(@RequestBody promoteur: Promoteur) = service.delete(promoteur)
}