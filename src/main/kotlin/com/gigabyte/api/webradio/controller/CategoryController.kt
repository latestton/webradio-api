package com.gigabyte.api.webradio.controller

import com.gigabyte.api.webradio.model.Category
import com.gigabyte.api.webradio.service.CategoryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class CategoryController {
    @Autowired
    lateinit var service : CategoryService
    @PostMapping("/category") fun post(@RequestBody category: Category) = service.saveCategory(category)
    @GetMapping("/categories") fun getAllCategorys(): Iterable<Category> = service.getAllCategories()
    @GetMapping("/category/{id}") fun getCategoryById(@PathVariable id: Int): Optional<Category> = service.getCategoryById(id)
    @PatchMapping("/category") fun patch(@RequestBody category: Category) = service.saveCategory(category)
    @DeleteMapping("/category") fun delete(@RequestBody category: Category) = service.delete(category)
}