package com.gigabyte.api.webradio.controller

import com.gigabyte.api.webradio.model.Owner
import com.gigabyte.api.webradio.service.OwnerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class OwnerController {
    @Autowired lateinit var service : OwnerService
    @PostMapping("/owner") fun post(@RequestBody owner: Owner) = service.saveOwner(owner)
    @GetMapping("/owners") fun getAllOwners(): Iterable<Owner> =service.getAllOwners()
    @GetMapping("/owner/{id}") fun getOwnerById( @PathVariable id: Int): Optional<Owner> = service.getOwnerById(id)
    @PatchMapping("/owner") fun patch(@RequestBody owner: Owner) = service.saveOwner(owner)
    @DeleteMapping("/owner") fun delete(@RequestBody owner: Owner) = service.delete(owner)
}