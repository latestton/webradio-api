package com.gigabyte.api.webradio

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HomeController {
    @GetMapping("/") fun index() : String {
        return "WEBRADIO API VERSION 1.0"
    }
}