package com.gigabyte.api.webradio

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("com.gigabyte.api.webradio")
class WebRadioApiApplication : CommandLineRunner {
    override fun run(vararg args: String?) {
        println("**************************************")
        println("*   WEB-RADIO API V 1.0 IS STARTED   *")
        println("**************************************")
    }
}

fun main(args: Array<String>) {
    runApplication<WebRadioApiApplication>(*args)
}
