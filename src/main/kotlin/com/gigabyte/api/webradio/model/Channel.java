package com.gigabyte.api.webradio.model;

import jakarta.persistence.GenerationType;
import lombok.Data;

import java.io.*;

@jakarta.persistence.Entity
@Data
public class Channel implements Serializable {

	Country country;
	Category category;
	Owner owner;
	private Integer id;
	private String name;
	private String url;

	@jakarta.persistence.ManyToOne(optional=false)
	@jakarta.persistence.JoinColumn(name="CountryID", referencedColumnName="ID", nullable=false)
	public Country getCountry() {
		return this.country;
	}

	/*public void setCountry(Country country) {
		this.country = country;
	}*/
	@jakarta.persistence.ManyToOne(optional=false)
	@jakarta.persistence.JoinColumn(name="OwnerID", referencedColumnName="ID", nullable=false)
	public Owner getOwner() {
		return this.owner;
	}

	@jakarta.persistence.ManyToOne(optional=false)
	@jakarta.persistence.JoinColumn(name="CategoryID", referencedColumnName="ID", nullable=false)
	public Category getCategory() {
		return this.category;
	}

	/*public void setCategory(Category category) {
		this.category = category;
	}*/

	@jakarta.persistence.Id
	@jakarta.persistence.GeneratedValue(strategy = GenerationType.IDENTITY)
	@jakarta.persistence.Column(name="ID", nullable=false, length=10)
	public Integer getId() {
		return this.id;
	}

	/*public void setId(Integer id) {
		this.id = id;
	}*/

	@jakarta.persistence.Column(name="Name")
	public String getName() {
		return this.name;
	}

	/*public void setName(String name) {
		this.name = name;
	}*/

	@jakarta.persistence.Column(name="Url")
	public String getUrl() {
		return this.url;
	}

	/*public void setUrl(String url) {
		this.url = url;
	}*/

}