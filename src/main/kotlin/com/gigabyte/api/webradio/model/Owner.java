package com.gigabyte.api.webradio.model;

import jakarta.persistence.FetchType;
import jakarta.persistence.GenerationType;
import lombok.Data;

import java.io.*;
import java.util.*;

@jakarta.persistence.Entity
@Data
public class Owner implements Serializable {
	Collection<Channel> channels;
	private Integer id;
	private String prenom;
	private String Nom;
	private String email;

	@jakarta.persistence.Id
	@jakarta.persistence.GeneratedValue(strategy= GenerationType.IDENTITY)
	@jakarta.persistence.Column(name="ID", nullable=false, length=10)
	public Integer getId() {
		return this.id;
	}

	@jakarta.persistence.OneToMany(mappedBy="owner",fetch= FetchType.EAGER)
	public Collection<Channel> getChannels() {
		return this.channels;
	}

	@jakarta.persistence.Column(name="Prenom")

	public String getPrenom() {
		return this.prenom;
	}

	@jakarta.persistence.Column(name="Nom")
	public String getNom() {
		return this.Nom;
	}
	@jakarta.persistence.Column(name="Email")
	public String getEmail() {
		return this.email;
	}


}