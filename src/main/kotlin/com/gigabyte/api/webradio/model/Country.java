package com.gigabyte.api.webradio.model;

import jakarta.persistence.FetchType;
import jakarta.persistence.GenerationType;

import java.io.*;
import java.util.*;

@jakarta.persistence.Entity
public class Country implements Serializable {

	Collection<Channel> channels;
	private Integer id;
	private String name;

	@jakarta.persistence.OneToMany(mappedBy="country",fetch= FetchType.EAGER)
	public Collection<Channel> getChannels() {
		return this.channels;
	}

	public void setChannels(Collection<Channel> channels) {
		this.channels = channels;
	}

	@jakarta.persistence.Id
	@jakarta.persistence.GeneratedValue(strategy= GenerationType.IDENTITY)
	@jakarta.persistence.Column(name="ID", nullable=false, length=10)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@jakarta.persistence.Column(name="Name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}